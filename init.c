/**
 *  Initialisierung für Blinker
 */

#include <stdint.h>
#include <stdio.h>
#include "stm32f1xx.h"

// This variable is used by some CMSIS functions



// Write standard output to the serial port 1
int _write(int file, char *ptr, int len)
{
    for (int i=0; i<len; i++)
    {
        while(!(USART1->SR & USART_SR_TXE));
        USART1->DR = *ptr++;
    }
    return len;
}

void init_io()
{
    // Enable Port A, B, C and alternate functions
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_IOPAEN);
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_IOPBEN);
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_IOPCEN);
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_AFIOEN);

    // Disable JTAG to free to free PA15, PB3 and PB4
    MODIFY_REG(AFIO->MAPR, AFIO_MAPR_SWJ_CFG, AFIO_MAPR_SWJ_CFG_JTAGDISABLE);

    // Configure the serial Port 1
    SET_BIT(RCC->APB2ENR, RCC_APB2ENR_USART1EN); // Enable
    MODIFY_REG(GPIOA->CRH, GPIO_CRH_CNF9 + GPIO_CRH_MODE9, GPIO_CRH_CNF9_1 + GPIO_CRH_MODE9_1); // PA9=Output for alternate function
    USART1->CR1 = USART_CR1_UE + USART_CR1_TE; // Enable transmitter (no receiver)
    USART1->BRR = (SystemCoreClock / 115200); // Set baud rate

    // PC13 = Output for the red LED
    MODIFY_REG(GPIOC->CRH, GPIO_CRH_CNF13 + GPIO_CRH_MODE13, GPIO_CRH_MODE13_0);
}
