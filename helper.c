#include "stm32f1xx.h"
#include "init.h"

// Milliseconds counter
volatile uint32_t systick_count=0;

// Interrupt handler for the system timer
void SysTick_Handler(void)
{
    systick_count++;
}

// warte x Sekunden
void wait(int dauer)
{
    // Initialize system timer
    SysTick_Config(SystemCoreClock/1000);

        // Delay 1 second * dauer
        uint32_t start=systick_count;
        while (systick_count-start<(1000*dauer));

}